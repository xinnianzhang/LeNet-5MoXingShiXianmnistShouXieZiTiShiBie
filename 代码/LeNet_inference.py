import tensorflow as tf
#定义神经网络的参数
INPUT_NODE = 784#输入层节点数
OUTPUT_NODE = 10#输出层节点数
IMAGE_SIZE = 28#像素尺寸
NUM_CHANNELS = 1#通道数
NUM_LABELS = 10#手写数字类别数目
#第一层卷积层的尺度和深度
CONV1_DEEP = 32
CONV1_SIZE = 5
#第二层卷积层的尺寸和深度
CONV2_DEEP = 64
CONV2_SIZE = 5
#全连接层的节点个数
FC_SIZE = 512
#定义卷积神经网络的前向传播过程
def inference(input_tensor,train,regularizer):
    #声明第一层卷积层的变量并实现前向传播的过程
    with tf.variable_scope('layer1-conv1'):
        conv1_weights = tf.get_variable('weight',[CONV1_SIZE,CONV1_SIZE,NUM_CHANNELS,CONV1_DEEP],
                                        initializer=tf.truncated_normal_initializer(stddev=0.1))
        conv1_biases = tf.get_variable('bias',[CONV1_DEEP],initializer=tf.constant_initializer(0.0))
        #使用尺寸为5x5 深度为32，步长为1，的过滤器且全0填充
        conv1 = tf.nn.conv2d(
            input_tensor,conv1_weights,strides=[1,1,1,1],padding='SAME')
        relul = tf.nn.relu(tf.nn.bias_add(conv1,conv1_biases))
    #实现第二层池化层的前向传播过程，这里选最大池化层
    #使用尺寸2x2，步长为2，且使用全0填充的过滤器，输入为上一层的输出 28x28x32
    with tf.name_scope('layer2-pool1'):
        pool1 = tf.nn.max_pool(
            relul,ksize=[1,2,2,1],strides=[1,2,2,1],padding='SAME')
    #声明第三层卷积层的变量并实现前向传播的过程
    #输入为14x14x32的矩阵，输出14x14x64的矩阵
    with tf.variable_scope('layer3-conv2'):
        conv2_weights = tf.get_variable('weight', [CONV2_SIZE, CONV2_SIZE, CONV1_DEEP, CONV2_DEEP],
                                        initializer=tf.truncated_normal_initializer(stddev=0.1))
        conv2_biases = tf.get_variable('bias', [CONV2_DEEP], initializer=tf.constant_initializer(0.0))
        # 使用尺寸为5x5 深度为64，步长为1，的过滤器且全0填充
        conv2 = tf.nn.conv2d(
            pool1,conv2_weights,strides=[1,1,1,1],padding='SAME')
        relu2 = tf.nn.relu(tf.nn.bias_add(conv2, conv2_biases))
    #第四层池化层的前向传播过程，与第二层一致，
    #输入为14x14x64的矩阵，输出为7x7x64的矩阵
    with tf.name_scope('layer4-pool2'):
        pool2= tf.nn.max_pool(
            relu2,ksize=[1,2,2,1],strides=[1,2,2,1],padding='SAME')
    #将第四层池化层的输出转化为第五层全连接层的输入格式，
    #第四层输出7x7x64的矩阵，第五层需要输入的格式为向量，所以把矩阵拉长成为向量
    pool_shape = pool2.get_shape().as_list()
    #计算将矩阵拉直成向量之后的长度，这个长度就是矩阵的长宽及深度的乘积
    nodes = pool_shape[1]*pool_shape[2]*pool_shape[3]
    #通过tf.reshape函数将第四层的输出变成一个batch的向量
    reshaped = tf.reshape(pool2,[pool_shape[0],nodes])
    #声明第五层全连接层的变量并实现前向传播过程
    with tf.variable_scope('layer5-fc1'):
        fc1_weights = tf.get_variable(
            'weight',[nodes,FC_SIZE],
            initializer=tf.truncated_normal_initializer(stddev=0.1))
        #只有全连接层需要正则化
        if regularizer != None:
            tf.add_to_collection('losses',regularizer(fc1_weights))
        fc1_biases = tf.get_variable(
                'bias',[FC_SIZE],
                initializer=tf.constant_initializer(0.1))
        fc1 = tf.nn.relu(tf.matmul(reshaped,fc1_weights)+fc1_biases)
        if train:fc1 = tf.nn.dropout(fc1,0.5)
    #声明第六层全连接层的变量并实现前向传播过程
    with tf.variable_scope('layer6-fc2'):
        fc2_weights = tf.get_variable(
            'weight', [ FC_SIZE,NUM_CHANNELS],
            initializer=tf.truncated_normal_initializer(stddev=0.1))
        # 只有全连接层需要正则化
        if regularizer != None:
            tf.add_to_collection('losses', regularizer(fc2_weights))
        fc2_biases = tf.get_variable(
            'bias', [NUM_LABELS],
            initializer=tf.constant_initializer(0.1))
        logit = tf.matmul(fc1,fc2_weights) + fc2_biases
    #返回第六层的输出
    return logit





