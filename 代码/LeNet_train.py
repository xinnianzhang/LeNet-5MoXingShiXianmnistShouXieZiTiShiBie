import os
import tensorflow as tf
import numpy as np
from tensorflow.examples.tutorials.mnist import input_data
#加载LeNet_inference.py中定义的常量和前向传播的函数
import LeNet_inference
#配置神经网络参数
BATCH_SIZE = 100
LEARNING_RATE_BASE = 0.8
LEARNING_RATE_DECAY = 0.99
REGULARAZTION_RATE = 0.0001
TRAINING_STEPS = 6000
MOVING_AVERAGE_DECAY = 0.99
#模型保存的路径和文件名
MODEL_SAVE_PATH = 'D:/dream/TensorFlowMNIST_data'
MODEL_NAME = 'model.ckpt'
def train(mnist):
    #定义输入输出placeholder
    x = tf.placeholder(
        tf.float32,[
            BATCH_SIZE,#第一维表示batch中样例的个数
            LeNet_inference.IMAGE_SIZE,# 第二维和第三维表示图片的尺寸
            LeNet_inference.IMAGE_SIZE,
            LeNet_inference.NUM_CHANNELS],#第四维表示图片深度，RGB图片深度为3
            name = 'x-input'
    )
    y_ = tf.placeholder(
        tf.float32,[None,LeNet_inference.OUTPUT_NODE ],
        name = 'y-input'
    )
    regularizer = tf.contrib.layers.l2_regularizer(REGULARAZTION_RATE)
    #直接使用LeNet_inference.py中的前向传播过程
    y = LeNet_inference.inference(x,True,regularizer)
    global_step = tf.Variable(0,trainable=False)
    #定义损失函数，学习率，滑动平均操作及训练过程
    variable_averages = tf.train.ExponentialMovingAverage(MOVING_AVERAGE_DECAY, global_step)
    variable_averages_op = variable_averages.apply(tf.trainable_variables())
    cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=y, labels=tf.argmax(y_, 1))
    cross_entropy_mean = tf.reduce_mean(cross_entropy)
    loss = cross_entropy_mean + tf.add_n(tf.get_collection('losses'))
    learning_rate = tf.train.exponential_decay(
        LEARNING_RATE_BASE,  # 基础的学习率，随着迭代的进行，更新变量时使用的，学习率在这个基础上递减。
        global_step,  # 当前迭代的轮数。
        mnist.train.num_examples / BATCH_SIZE,  # 过完所有的训练数据需要的迭代次数。
        LEARNING_RATE_DECAY,  # 学习率衰减速度
        staircase=True

    )
    train_step = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss, global_step=global_step)
    with tf.control_dependencies([train_step,variable_averages_op]):
        train_op = tf.no_op(name='train')

    #初始化TensorFlow持久化类
    saver = tf.train.Saver()
    with tf.Session() as sess:
        tf.global_variables_initializer().run()
        #在训练过程中不在测试模型在验证数据上的表现，验证和测试的过程将会有一个独立的程序来完成
        for i in range(TRAINING_STEPS):
            xs,ys = mnist.train.next_batch(BATCH_SIZE)
            reshaped_xs = np.reshape(xs,(BATCH_SIZE,
                                     LeNet_inference.IMAGE_SIZE,
                                     LeNet_inference.IMAGE_SIZE,
                                     LeNet_inference.NUM_CHANNELS))
            _,loss_value,step = sess.run([train_op,loss,global_step],feed_dict = {x:reshaped_xs,y_:ys})
            #每1000轮保存一次模型
            if i % 1000 == 0:
                #输出当前的训练情况
                print('After %d training step(s),loss on training' 'batch is %g.' % (step,loss_value))
                #保存当前模型
                saver.save(
                    sess,os.path.join(MODEL_SAVE_PATH,MODEL_NAME),global_step = global_step)
def main(argv=None):
    mnist = input_data.read_data_sets('D:/dream/TensorFlow/MNIST_data',one_hot=True)
    train(mnist)
if __name__ == '__main__':
    tf.app.run()
