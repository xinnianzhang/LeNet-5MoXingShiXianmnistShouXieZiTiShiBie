import time
import tensorflow as tf
import numpy as np
from tensorflow.examples.tutorials.mnist import input_data
#加载mnist_inference.py和mnist_train.py中定义的常量和函数
import LeNet_inference
import LeNet_train
#每10秒加载一次最新的模型，并在测试数据上测试最新模型的正确率
EVAL_INTERVAL_SECS = 10
def evaluate(mnist):
    with tf.Graph().as_default() as g:#将默认图设置为g
        #定义输入输出的格式
        x = tf.placeholder(
            tf.float32,[mnist.validation.images.shape[0],
                        LeNet_inference.IMAGE_SIZE,
                        LeNet_inference.IMAGE_SIZE,
                        LeNet_inference.NUM_CHANNELS],name='x-input'
        )
        y_ = tf.placeholder(
            tf.float32, [None, LeNet_inference.OUTPUT_NODE], name='y-input'
        )
        xs = mnist.validation.images
        reshaped_xs = np.reshape(xs, (mnist.validation.images.shape[0],
                                      LeNet_inference.IMAGE_SIZE,
                                      LeNet_inference.IMAGE_SIZE,
                                      LeNet_inference.NUM_CHANNELS))
        validate_feed = {x:reshaped_xs,
                         y_:mnist.validation.labels}
        #直接通过调用好的函数来计算前向传播的结果，因为测试时不关注正则化损失的值，所以这里设置为None
        y = LeNet_inference.inference(x,None,None)
        #使用前向传播的结果计算正确率
        correct_prediction = tf.equal(tf.argmax(y,1),tf.argmax(y_,1))
        accuracy = tf.reduce_mean(tf.cast(correct_prediction,tf.float32))
        #通过变量重命名来加载模型
        variable_averages = tf.train.ExponentialMovingAverage(LeNet_train.MOVING_AVERAGE_DECAY)
        variables_to_restore = variable_averages.variables_to_restore()
        saver = tf.train.Saver(variables_to_restore)
        #每隔Eval_INTERVAL_SECS秒调用一次计算正确率的过程以检测训练过程中正确率的变化
        while True:
            with tf.Session() as sess:
                ckpt = tf.train.get_checkpoint_state(LeNet_train.MODEL_SAVE_PATH)
                if ckpt and ckpt.model_checkpoint_path:
                    #加载模型
                    saver.restore(sess,ckpt.model_checkpoint_path)
                    #通过文件名得到模型保存时的迭代的轮数
                    global_step = ckpt.model_checkpoint_path.split('/')[-1]
                    accuracy_score = sess.run(accuracy,feed_dict=validate_feed)
                    print('After %s training step(s),validation'  'accuracy=%g'%(global_step,accuracy_score))
                else:
                    print('No checkpoint file found')
                    return
                time.sleep(EVAL_INTERVAL_SECS)
def main(argv=None):
    mnist = input_data.read_data_sets('D:/dream/TensorFlow/MNIST_data',one_hot=True)
    evaluate(mnist)
if __name__ == '__main__':
    tf.app.run()
